cabal-version: 1.12

-- This file has been generated from package.yaml by hpack version 0.35.0.
--
-- see: https://github.com/sol/hpack

name:           homplexity
version:        0.4.8.1
synopsis:       Haskell code quality tool
description:    Homplexity aims to measure code complexity,
                warning about fragments that might have higher defect probability
                due to bad coding style on-the-large:
                .
                * too large functions
                .
                * too deeply nested conditions
                .
                * too few comments
category:       Language, Tools
homepage:       https://gitlab.com/migamake/homplexity
bug-reports:    https://gitlab.com/migamake/homplexity/issues
author:         Michal J. Gajda
maintainer:     mjgajda@migamake.com
copyright:      Copyright by Michal J. Gajda and contributors '2015-'2020 https://gitlab.com/migamake/homplexity/-/graphs/master
license:        BSD3
license-file:   LICENSE
build-type:     Simple
tested-with:
    GHC==8.4.4 GHC==8.6.5 GHC==8.8.2 GHC==8.10.4 GHC==9.0 GHC==9.2 GHC==9.4
extra-source-files:
    README.md
    changelog.md

source-repository head
  type: git
  location: https://gitlab.com/migamake/homplexity.git

flag html
  description: Enable HTML output via blaze-html
  manual: False
  default: True

flag static
  description: Build static binary
  manual: True
  default: False

library
  exposed-modules:
      Language.Haskell.Homplexity.Assessment
      Language.Haskell.Homplexity.CabalFiles
      Language.Haskell.Homplexity.CodeFragment
      Language.Haskell.Homplexity.Comments
      Language.Haskell.Homplexity.Cyclomatic
      Language.Haskell.Homplexity.Message
      Language.Haskell.Homplexity.Metric
      Language.Haskell.Homplexity.Parse
      Language.Haskell.Homplexity.SrcSlice
      Language.Haskell.Homplexity.RecordFieldsCount
      Language.Haskell.Homplexity.TypeClassComplexity
      Language.Haskell.Homplexity.TypeComplexity
      Language.Haskell.Homplexity.Utilities
  other-modules:
      Paths_homplexity
  hs-source-dirs:
      lib
  other-extensions:
      BangPatterns
      DeriveDataTypeable
      FlexibleContexts
      FlexibleInstances
      GeneralizedNewtypeDeriving
      IncoherentInstances
      MultiParamTypeClasses
      OverlappingInstances
      RecordWildCards
      ScopedTypeVariables
      StandaloneDeriving
      TemplateHaskell
      TypeFamilies
      TypeSynonymInstances
      UndecidableInstances
      ViewPatterns
  build-tools:
      alex
    , happy
  build-depends:
      base >=4.5 && <4.19
    , bytestring
    , containers >=0.3 && <0.7
    , cpphs >=1.5 && <1.21
    , deepseq >=1.3 && <1.7
    , directory >=1.1 && <1.4
    , filepath >=1.2 && <1.5
    , haskell-src-exts >=1.20 && <1.24
    , hflags >=0.3 && <0.5
    , template-haskell >=2.6 && <2.20
    , uniplate >=1.4 && <1.7
  default-language: Haskell2010
  if flag(static)
    ghc-options: -fPIC
    ld-options: -static
  if impl(ghc >=8.8.1)
    build-depends:
        Cabal >=3.0.0.0
  else
    build-depends:
        Cabal >=2.2.0.0
  if flag(html)
    cpp-options: -DHTML_OUTPUT
    build-depends:
        blaze-html >=0.9 && <1
      , blaze-markup ==0.8.*

executable homplexity-cli
  main-is: Homplexity.hs
  other-modules:
      Paths_homplexity
  hs-source-dirs:
      app/
  build-depends:
      base >=4.5 && <4.19
    , containers >=0.3 && <0.7
    , cpphs >=1.5 && <1.21
    , deepseq >=1.3 && <1.7
    , directory >=1.1 && <1.4
    , filepath >=1.2 && <1.5
    , githash >=0.1.2.0 && <1.0
    , haskell-src-exts >=1.20 && <1.24
    , hflags >=0.3 && <0.5
    , homplexity
    , template-haskell >=2.6 && <2.20
    , uniplate >=1.4 && <1.7
  default-language: Haskell2010
  if flag(static)
    ghc-options: -fPIC
    ld-options: -static
  if flag(html)
    cpp-options: -DHTML_OUTPUT
    build-depends:
        blaze-html >=0.9 && <1
      , bytestring ==0.10.*

test-suite homplexity-tests
  type: exitcode-stdio-1.0
  main-is: Tests.hs
  other-modules:
      Test.Metrics.TypeClassComplexitySpec
      Test.Parse.CommentsSpec
      Test.Parse.ExtensionsSpec
      Test.Utilities
      Test.Utils
      Paths_homplexity
  hs-source-dirs:
      tests
  ghc-options: -Wall -threaded -rtsopts -with-rtsopts=-N
  build-depends:
      base >=4.5 && <4.19
    , filepath >=1.2 && <1.5
    , haskell-src-exts >=1.20 && <1.24
    , homplexity
    , template-haskell >=2.6 && <2.20
  default-language: Haskell2010
  if flag(static)
    ghc-options: -fPIC
    ld-options: -static
  if impl(ghc >=8.8.0)
    build-tool-depends:
        hspec-discover:hspec-discover
    build-depends:
        hspec >=2.7.0
  else
    build-depends:
        hspec <2.6.0
