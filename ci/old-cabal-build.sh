#!/bin/bash

source ci/common.sh

message "Dependencies"
cabal update
cabal v1-install --dependencies-only --enable-tests

message "Build"
cabal v1-configure --enable-tests --allow-newer
cabal v1-build
cabal v1-test

message "Prepare release artifacts"
mkdir -p bin sdist
cabal v1-install --bindir=bin/
cabal v1-sdist   --builddir=sdist/
cabal v1-haddock --builddir hackage-docs --for-hackage

message "Run on own source"
bin/homplexity-cli lib/
