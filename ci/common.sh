#!/bin/bash

message () {
  echo -e "\e[1m\e[33m${*}\e[0m"
}

check_version() {
  if [ -x "`which $1`" ]; then
    "$1" --version;
  else
    echo "No $1 available.";
  fi
}

set -o verbose
set -o xtrace
set -o pipefail
set -o errexit
set -o nounset

message "Versions"
check_version cabal
check_version ghc

